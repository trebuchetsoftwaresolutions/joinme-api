var express = require('express');
var router = express();
var sqlConnection = require('../dbConSQL');
var tokenHandler = require('../sessionTokenHandler');

router.put('/newuser', function (req, res, next) {
    var data = req.body;
    console.log(data);
    var statement = 'INSERT INTO users (Username, Email, Password, Role, Description, ' +
        'Interests, Gender, PrimaryLang, OriginCountry, BirthDate, ProfilePic, Active) VALUES("' + data.username + '", "' + data.email + '", SHA1("' + data.password + '"), "' + data.role + '", "' + data.description + '", "' + data.interests + '", "' + data.gender + '",' +
        ' "' + data.primaryLang + '", "' + data.originCountry + '", "' + data.birthDate + '", "' + data.profilePic + '", "' + data.active + '");';
    console.log('QUERY SQL -> ', statement);
    sqlConnection.query(statement, function (err, result) {
        if (err) throw err;
        console.log('RES ->', result);
        if (result.affectedRows === 0) {
            res.sendStatus(400);
        } else {
            res.sendStatus(201);
        }
    });
});

router.put('/toggleactive', async function (req, res, next) {
    var data = req.body;
    var headers = req.headers;
    try {
        await tokenHandler.checkAuthToken(headers.token).then(response => {
            if (response) {
                console.log(data);
                var statement = 'UPDATE users SET Active=' + data.active + ' WHERE UserID="' + data.userId + '";';
                console.log('QUERY SQL -> ', statement);
                sqlConnection.query(statement, function (err, result) {
                    if (err) throw err;
                    console.log('RES ->', result);
                    if (result.affectedRows === 0) {
                        res.sendStatus(204);
                    } else {
                        res.sendStatus(200);
                    }
                });
            } else {
                res.sendStatus(401);
            }
        });
    } catch (err) {
        throw new Error(err);
    }
});

router.post('/user', async function (req, res, next) {
    var data = req.body;
    var headers = req.headers;
    await tokenHandler.checkAuthToken(headers.token).then(response => {
        if (response) {
            console.log(data);
            var statement = 'SELECT * FROM users WHERE UserID ="' + data.id + '";';
            console.log('QUERY SQL -> ' + statement);
            sqlConnection.query(statement, function (err, result) {
                if (err) throw err;
                console.log('RES ->', result);
                res.send(result);
            });
        } else {
            res.sendStatus(401);
        }
    });
});

router.get('/users', async function (req, res, next) {
    var headers = req.headers;
    await tokenHandler.checkAuthToken(headers.token).then(response => {
        if (response) {
            var statement = 'SELECT * FROM users;';
            console.log('QUERY SQL -> ' + statement);
            sqlConnection.query(statement, function (err, result) {
                if (err) {
                    throw err;
                } else {
                    if (result.length) {
                        console.log('RES ->', result);
                        res.send(result);
                    } else {
                        res.sendStatus(204);
                    }
                }
            });
        } else {
            res.sendStatus(401);
        }
    });
});

router.post('/login', function (req, res, next) {
    var data = req.body;
    console.log(req.body);
    var statement = 'SELECT * FROM users WHERE Email ="' + data.email + '" AND Password =SHA1("' + data.password + '");';
    console.log('QUERY SQL -> ' + statement);
    sqlConnection.query(statement, async function (err, result) {
        if (err) {
            throw err;
        } else {
            await tokenHandler.newAuthToken(result[0].UserID).then(token => {
                console.log(token);
                if (result.length) {
                    res.send({"user": result[0], "token": token});
                } else {
                    res.sendStatus(400);
                }
            });

        }
    });
});

router.post('/checkusername', async function (req, res) {
    var data = req.body;
    var headers = req.headers;
    await tokenHandler.checkAuthToken(headers.token).then(response => {
        if (response) {
            console.log(req.body);
            var statement = 'SELECT Username FROM users WHERE Username ="' + data.username + '";';
            console.log('QUERY SQL -> ' + statement);
            sqlConnection.query(statement, function (err, result) {
                if (err) {
                    throw err;
                } else {
                    console.log('RES ->', result);
                    res.send(result);
                }
            });
        } else {
            res.sendStatus(204);
        }
    });
});

router.put('/updateuser', async function (req, res, next) {
    var data = req.body;
    var headers = req.headers;
    await tokenHandler.checkAuthToken(headers.token).then(response => {
        if (response) {
            console.log(data);
            var statement = 'UPDATE users SET Username="' + data.username + '", Email="' + data.email + '", Role="' + data.role + '", Description="' + data.description + '", Interests="' + data.interests + '", Gender="' + data.gender + '",' +
                ' PrimaryLang="' + data.primaryLang + '", OriginCountry="' + data.originCountry + '", BirthDate="' + data.birthDate + '", ProfilePic="' + data.profilePic + '", Active="' + data.active + '" WHERE UserID="' + data.userId + '";';
            console.log('QUERY SQL -> ', statement);
            sqlConnection.query(statement, function (err, result) {
                if (err) throw err;
                console.log('RES ->', result);
                if (result.affectedRows === 0) {
                    res.sendStatus(204);
                } else {
                    res.sendStatus(200);
                }
            });
        } else {
            res.sendStatus(401);
        }
    });
});

router.put('/recoverpass', async function (req, res, next) {
    var data = req.body;
    var headers = req.headers;
    await tokenHandler.checkAuthToken(headers.token).then(response => {
        if (response) {
            console.log(data);
            var statement = 'UPDATE users SET Password= SHA1("' + data.newPassword + '") WHERE Email="' + data.email + '" AND Password= SHA1("' + data.oldPassword + '");';
            console.log('QUERY SQL -> ' + statement);
            sqlConnection.query(statement, function (err, result) {
                if (err) throw err;
                console.log('RES ->', result);
                if (result.affectedRows === 0) {
                    res.sendStatus(204);
                } else {
                    res.sendStatus(200);
                }
            });
        } else {
            res.sendStatus(401);
        }
    });
});

router.delete('/deleteuser', async function (req, res, next) {
    var data = req.body;
    var headers = req.headers;
    await tokenHandler.checkAuthToken(headers.token).then(response => {
        if (response) {
            console.log(data);
            var statement = 'DELETE FROM users WHERE UserID="' + data.id + '";';
            console.log('QUERY SQL -> ' + statement);
            sqlConnection.query(statement, function (err, result) {
                if (err) throw err;
                console.log('RES ->', result);
                if (result.affectedRows === 0) {
                    res.sendStatus(204);
                } else {
                    res.sendStatus(200);
                }
            });
        } else {
            res.sendStatus(401);
        }
    });
});


module.exports = router;
