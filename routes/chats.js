var express = require('express');
var router = express();
var sqlConnection = require('../dbConSQL');
var tokenHandler = require ('../sessionTokenHandler');

router.put('/newchat', async function (req, res, next) {
    var data = req.body;
    var headers = req.headers;
    await tokenHandler.checkAuthToken(headers.token).then(response => {
        if (response) {
            console.log(data);
            var statement = 'INSERT INTO chats (User1ID, User2ID, TripIDuser1, TripIDuser2) ' +
                'VALUES("' + data.user1id + '", "' + data.user2id + '", "' + data.tripIdUser1 + '", "' + data.tripIdUser2 + '");';
            console.log('QUERY SQL -> ', statement);
            sqlConnection.query(statement, function (err, result) {
                if (err) throw err;
                console.log('RES ->', result);
                if (result.affectedRows === 0) {
                    res.sendStatus(400);
                } else {
                    res.sendStatus(201);
                }
            });
        } else {
            res.sendStatus(401);
        }
    });
});


router.delete('/deletechat', async function (req, res, next) {
    var data = req.body;
    var headers = req.headers;
    await tokenHandler.checkAuthToken(headers.token).then(response => {
        if (response) {
            console.log(data);
            var statement = 'DELETE FROM chats WHERE ChatID="' + data.id + '";';
            console.log('QUERY SQL -> ' + statement);
            sqlConnection.query(statement, function (err, result) {
                if (err) throw err;
                console.log('RES ->', result);
                if (result.affectedRows === 0) {
                    res.sendStatus(204);
                } else {
                    res.sendStatus(200);
                }
            });
        } else {
            res.sendStatus(401);
        }
    });
});

router.get('/chats', async function (req, res, next) {
    var headers = req.headers;
    await tokenHandler.checkAuthToken(headers.token).then(response => {
        if (response) {
            var statement = 'SELECT * FROM chats;';
            console.log('QUERY SQL -> ' + statement);
            sqlConnection.query(statement, function (err, result) {
                if (err) throw err;
                console.log('RES ->', result);
                res.send(result);
            });
        } else {
            res.sendStatus(401);
        }
    });
});

router.post('/chatsbyuser', async function (req, res, next) {
    var data = req.body;
    var headers = req.headers;
    await tokenHandler.checkAuthToken(headers.token).then(response => {
        if (response) {
            console.log(data);
            var statement = 'SELECT * FROM chats WHERE User1ID="' + data.userId + '" OR User2ID="' + data.userId + '";';
            console.log('QUERY SQL -> ' + statement);
            sqlConnection.query(statement, function (err, result) {
                if (err){
                    throw err;
                } else {
                    if(result.length){
                        console.log('RES ->', result);
                        res.send(result);
                    } else {
                        res.sendStatus(204);
                    }
                }
            });
        } else {
            res.sendStatus(401);
        }
    });
});

router.post('/chat', async function (req, res, next) {
    var data = req.body;
    var headers = req.headers;
    await tokenHandler.checkAuthToken(headers.token).then(response => {
        if (response) {
            console.log(data);
            var statement = 'SELECT * FROM chats WHERE ChatID="' + data.id + '";';
            console.log('QUERY SQL -> ' + statement);
            sqlConnection.query(statement, function (err, result) {
                if (err) throw err;
                console.log('RES ->', result);
                res.send(result);
            });
        } else {
            res.sendStatus(401);
        }
    });
});

module.exports = router;