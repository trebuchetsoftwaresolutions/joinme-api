var express = require('express');
var router = express();
var tokenHandler = require('../sessionTokenHandler');
var sqlConnection = require('../dbConSQL');

router.put('/newtrip', async function (req, res, next) {
    var data = req.body;
    var headers = req.headers;
    await tokenHandler.checkAuthToken(headers.token).then(response => {
        if (response) {
            console.log(data);
            var statement = 'INSERT INTO trips (TripName, Country, Province, City, startDate, endDate, UserID) ' +
                'VALUES("' + data.tripName + '", "' + data.country + '", "' + data.province + '", "' + data.city + '", "' + data.startDate + '", "' + data.endDate + '", "' + data.userId + '");';
            console.log('QUERY SQL -> ', statement);
            sqlConnection.query(statement, function (err, result) {
                if (err) throw err;
                console.log('RES ->', result);
                if (result.affectedRows === 0) {
                    res.sendStatus(400);
                } else {
                    res.sendStatus(201);
                }
            });
        } else {
            res.sendStatus(401);
        }
    });

});


router.put('/updatetrip', async function (req, res, next) {
    var data = req.body;
    var headers = req.headers;
    await tokenHandler.checkAuthToken(headers.token).then(response => {
        if (response) {
            console.log(data);
            var statement = 'UPDATE trips SET TripName="' + data.tripName + '", Country="' + data.country + '", Province="' + data.province + '", City="' + data.city + '", startDate="' + data.startDate + '", endDate="' + data.endDate + '" WHERE TripID="' + data.tripId + '";';
            console.log('QUERY SQL -> ', statement);
            sqlConnection.query(statement, function (err, result) {
                if (err) throw err;
                console.log('RES ->', result);
                if (result.affectedRows === 0) {
                    res.sendStatus(204);
                } else {
                    res.sendStatus(200);
                }
            });
        } else {
            res.sendStatus(401);
        }
    });
});

router.delete('/deletetrip', async function (req, res, next) {
    var data = req.body;
    var headers = req.headers;
    await tokenHandler.checkAuthToken(headers.token).then(response => {
        if (response) {
            console.log(data);
            var statement = 'DELETE FROM trips WHERE TripID="' + data.id + '";';
            console.log('QUERY SQL -> ' + statement);
            sqlConnection.query(statement, function (err, result) {
                if (err) throw err;
                console.log('RES ->', result);
                if (result.affectedRows === 0) {
                    res.sendStatus(204);
                } else {
                    res.sendStatus(200);
                }
            });
        } else {
            res.sendStatus(401);
        }
    });
});

router.get('/trips', async function (req, res, next) {
    var headers = req.headers;
    await tokenHandler.checkAuthToken(headers.token).then(response => {
        if (response) {
            var statement = 'SELECT * FROM trips;';
            console.log('QUERY SQL -> ' + statement);
            sqlConnection.query(statement, function (err, result) {
                if (err) throw err;
                console.log('RES ->', result);
                res.send(result);
            });
        } else {
            res.sendStatus(401);
        }
    });
});

router.post('/tripsbyuser', async function (req, res, next) {
    var data = req.body;
    var headers = req.headers;
    await tokenHandler.checkAuthToken(headers.token).then(response => {
        if (response) {
            console.log(data);
            var statement = 'SELECT * FROM trips WHERE UserID="' + data.userId + '";';
            console.log('QUERY SQL -> ' + statement);
            sqlConnection.query(statement, function (err, result) {
                if (err){
                    throw err;
                } else {
                    if(result.length){
                        console.log('RES ->', result);
                        res.send(result);
                    } else {
                        res.sendStatus(204);
                    }
                }
            });
        } else {
            res.sendStatus(401);
        }
    });
});

router.post('/trip', async function (req, res, next) {
    var data = req.body;
    var headers = req.headers;
    await tokenHandler.checkAuthToken(headers.token).then(response => {
        if (response) {
            console.log(data);
            var statement = 'SELECT * FROM trips WHERE TripID="' + data.id + '";';
            console.log('QUERY SQL -> ' + statement);
            sqlConnection.query(statement, function (err, result) {
                if (err){
                    throw err;
                } else {
                    if(result.length){
                        console.log('RES ->', result);
                        res.send(result);
                    } else {
                        res.sendStatus(204);
                    }
                }
            });
        } else {
            res.sendStatus(401);
        }
    });
});

module.exports = router;