var express = require('express');
var router=express.Router();
var Meetup = require('meetup');
var mup = new Meetup();
var control=false;
var mongoose = require('mongoose');
var dbCon = require('../dbConMON');
var meetupMdl = require('../models/message.mdl');
var meetupEvent = mongoose.model('meetupEvent',meetupMdl);


router.get('/', function(req, res, next){
    var io = require('../bin/www');
    //Meetup stream capturing
    var count=0;
    if (!control) {
        io.on('connection', function (socket) {
            control=true;
            console.log('Conectado con socket');
            mup.stream('/2/rsvps', function (stream) {
                socket.on('capture', (message) => {
                    if (message === 'start') {
                        console.log("Start");
                        stream.on('data', function (item) {
                            // console.log(JSON.stringify(item));
                            count++;
                            var actualEvent = new meetupEvent({});
                            var auxTime= new Date(item.event.time);
                            var date= auxTime.getUTCFullYear()+'-'+(auxTime.getUTCMonth()+1)+'-'+auxTime.getUTCDate();
                            if(item.venue === undefined){
                                actualEvent = new meetupEvent({
                                    eventName: item.event.event_name,
                                    userName: item.member.member_name,
                                    userPhoto: item.member.photo,
                                    city: item.group.group_city,
                                    countryCode: item.group.group_country,
                                    eventTimestamp: item.event.time,
                                    eventTime: date,
                                    venueLat: 0,
                                    venueLon: 0,
                                    eventLink: item.event.event_url
                                });
                            }else{
                                actualEvent = new meetupEvent({
                                    eventName: item.event.event_name,
                                    userName: item.member.member_name,
                                    userPhoto: item.member.photo,
                                    city: item.group.group_city,
                                    countryCode: item.group.group_country,
                                    eventTimestamp: item.event.time,
                                    eventTime: date,
                                    venueLat: item.venue.lat,
                                    venueLon: item.venue.lon,
                                    eventLink: item.event.event_url
                                });
                            }

                            meetupEvent.findOneAndUpdate({eventName:actualEvent.eventName, userName:actualEvent.userName, eventTime:actualEvent.eventTime, eventTimestamp:actualEvent.eventTimestamp},actualEvent, {new: true, upsert: true}, function(err,actualEvent){
                                if (err){
                                    return console.error("Elemento ya existente en BBDD. No insertado");
                                } else{
                                    // console.log('Elemento guardado',actualEvent);
                                    console.log('Elemento guardado en BBDD');
                                    socket.emit('stream', item);
                                }
                            });

                        }).on('error', function (error) {
                            console.log('Error en el Stream');
                            console.error(error);
                        });
                    } else if (message == 'stop') {
                        stream.destroy();
                        socket.disconnect(true);
                        console.log("Stop");
                        console.log('Se han capturado '+count+' eventos en total');
                        count=0;

                    }
                });
            });
        });
    }
});
module.exports = router;