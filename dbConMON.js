var mongoose = require('mongoose');
var con=null;
if (con==null) {
    function conexionMON() {
        mongoose.connect('mongodb://localhost');
        var db=mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:'));
        db.once('open', function() {
            console.log("Conectado a la BBDD MongoDB");
            return db;
        });
    };
}
module.exports= conexionMON();