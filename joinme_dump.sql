# ************************************************************
# Sequel Pro SQL dump
# Version 5425
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 8.0.13)
# Database: joinme
# Generation Time: 2018-11-21 18:18:38 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table chats
# ------------------------------------------------------------

DROP TABLE IF EXISTS `chats`;

CREATE TABLE `chats` (
  `ChatID` int(11) NOT NULL AUTO_INCREMENT,
  `User1ID` int(11) NOT NULL,
  `User2ID` int(11) NOT NULL,
  `TripIDuser1` int(11) NOT NULL,
  `TripIDuser2` int(11) NOT NULL,
  PRIMARY KEY (`ChatID`),
  KEY `User1ID` (`User1ID`),
  KEY `User2ID` (`User2ID`),
  KEY `TripIDUser1` (`TripIDuser1`) USING BTREE,
  KEY `chats_ibfk_4` (`TripIDuser2`),
  CONSTRAINT `chats_ibfk_1` FOREIGN KEY (`User1ID`) REFERENCES `users` (`userid`),
  CONSTRAINT `chats_ibfk_2` FOREIGN KEY (`User2ID`) REFERENCES `users` (`userid`),
  CONSTRAINT `chats_ibfk_3` FOREIGN KEY (`TripIDuser1`) REFERENCES `trips` (`tripid`),
  CONSTRAINT `chats_ibfk_4` FOREIGN KEY (`TripIDuser2`) REFERENCES `trips` (`tripid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table session_token
# ------------------------------------------------------------

DROP TABLE IF EXISTS `session_token`;

CREATE TABLE `session_token` (
  `token` varchar(40) NOT NULL DEFAULT '',
  `UserID` int(11) NOT NULL,
  PRIMARY KEY (`token`),
  KEY `UserID` (`UserID`),
  CONSTRAINT `session_token_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `users` (`userid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `session_token` WRITE;
/*!40000 ALTER TABLE `session_token` DISABLE KEYS */;

INSERT INTO `session_token` (`token`, `UserID`)
VALUES
	('0ab8b4fd48d053d801d22d7210bc03942bfa6e5e',14),
	('5535af345e05496d3b7f4060f715d635ddfac188',14);

/*!40000 ALTER TABLE `session_token` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trips
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trips`;

CREATE TABLE `trips` (
  `TripID` int(11) NOT NULL AUTO_INCREMENT,
  `TripName` varchar(15) NOT NULL,
  `Country` varchar(15) NOT NULL,
  `Province` varchar(15) NOT NULL,
  `City` varchar(15) NOT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `UserID` int(11) NOT NULL,
  PRIMARY KEY (`TripID`),
  KEY `UserID` (`UserID`),
  CONSTRAINT `trips_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `users` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(15) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `Password` varchar(40) NOT NULL,
  `Role` varchar(10) NOT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Interests` varchar(120) DEFAULT NULL,
  `Gender` varchar(15) NOT NULL,
  `PrimaryLang` varchar(15) NOT NULL,
  `OriginCountry` varchar(15) NOT NULL,
  `BirthDate` date NOT NULL,
  `CreationDate` date DEFAULT NULL,
  `ModificationDate` date DEFAULT NULL,
  `LastLoginDate` date DEFAULT NULL,
  `ProfilePic` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) NOT NULL,
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `Username` (`Username`),
  UNIQUE KEY `uniqueEmail` (`Email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`UserID`, `Username`, `Email`, `Password`, `Role`, `Description`, `Interests`, `Gender`, `PrimaryLang`, `OriginCountry`, `BirthDate`, `CreationDate`, `ModificationDate`, `LastLoginDate`, `ProfilePic`, `Active`)
VALUES
	(14,'Me','demo@gmail.com','7c222fb2927d828af22f592134e8932480637c0d','user','Description test','Interest1','m','es','es','0000-00-00',NULL,NULL,NULL,'blablabla.jpg',1),
	(16,'Myself','demo2@gmail.com','7c222fb2927d828af22f592134e8932480637c0d','user','Looooooong description test','Interest1, Interest2','w','es','es','1990-01-01',NULL,NULL,NULL,'blablabla.jpg',1);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
