var sqlConnection = require('./dbConSQL');
module.exports = {
    getAuthTokenFromUserID: function getAuthTokenFromUserID(id) {
        var checkStatement = 'SELECT * FROM session_token WHERE UserID="' + id + '";';
        sqlConnection.query(checkStatement, function (err, result) {
            if (err) {
                throw err;
            } else {
                if (result[0]) {
                    return result[0];
                } else {
                    return 204;
                }
            }

        });
    },

    newAuthToken: function newAuthToken(id) {
        return new Promise(resolve => {
            if (module.exports.checkAuthTokenFromUserID(id)) {
                sqlConnection.query('UPDATE session_token SET token=SHA1("' + Math.floor(Math.random() * (9999 - 1111)) + 1111 + '") WHERE UserID ="' + id + '";', function (err, result) {
                    if (err) {
                        throw err;
                    } else {
                        setTimeout(() => {
                            resolve(module.exports.getAuthTokenFromUserID(id));
                        }, 1000);
                    }

                });
            } else {
                sqlConnection.query('INSERT INTO session_token (UserID, token) VALUES ("' + id + '", SHA1("' + Math.floor(Math.random() * (9999 - 1111)) + 1111 + '"));', function (err, result) {
                    if (err) {
                        throw err;
                    } else {
                        setTimeout(() => {
                            resolve(module.exports.getAuthTokenFromUserID(id));
                        }, 1000);
                    }

                });
            }
        });
    },

    checkAuthToken: function checkAuthToken(token) {
        return new Promise(resolve => {
            var checkStatement = 'SELECT * FROM session_token WHERE token="' + token + '";';
            sqlConnection.query(checkStatement, function (err, result) {
                if (err) {
                    throw err;
                } else {
                    if (result[0]) {
                        setTimeout(() => {
                            resolve(true);
                        }, 100);
                    }
                    else {
                        setTimeout(() => {
                            resolve(false);
                        }, 100);
                    }
                }

            });
        });
    },

    checkAuthTokenFromUserID: function checkAuthTokenFromUserID(id) {
        var checkStatement = 'SELECT * FROM session_token WHERE UserID="' + id + '";';
        sqlConnection.query(checkStatement, function (err, result) {
            if (err) {
                throw err;
            } else {
                if (result[0]) {
                    return true;
                }
                else {
                    return false;
                }
            }

        });

    }

};